import os
import time
import git
import socket
import requests
from pyA20.gpio import gpio
from pyA20.gpio import port

# constants
WIFI_CHECK_URL = "www.google.com"
SW_VERSION_UPDATE_URL = "https://petiary.coxlab.kr/update-software-version"

ON = 0
OFF = 1
PRESS = 0
RELEASE = 1

# GPIO 
btn = port.PD14
redLed = port.PA6
greenLed = port.PA3

# global
serialKey = ""

#================== functions ===================
def init():
    gpio.init()
    gpio.setcfg(btn, gpio.INPUT)
    gpio.setcfg(redLed, gpio.OUTPUT)
    gpio.setcfg(greenLed, gpio.OUTPUT)
    time.sleep(0.1)
    gpio.pullup(btn, gpio.PULLDOWN)
    GREEN(OFF)
    RED(OFF)

def readBtn():
    return gpio.input(btn)

def GREEN(x):
    gpio.output(greenLed, x)

def RED(x):
    gpio.output(redLed, x)

def blinkLED():
    for x in range(0, 5):
        GREEN(ON)
        RED(ON)
        time.sleep(0.5)
        GREEN(OFF)
        RED(OFF)
        time.sleep(0.5)

def isConnected():
    try:
        host = socket.gethostbyname(WIFI_CHECK_URL)
        s = socket.create_connection((host, 80), 2)
        return True
    except:
        pass
    return False

def loadSerialKey():
    global serialKey
    try:
        f = open("/petiary/petd/SerialKey", 'r')
        serialKey = f.read().splitlines()[0]
        f.close()
    except:
        print "failed loading serial key" 

def notifySWVersion(swVersion):
    data = { 'sn' : serialKey, 'swVersion' : swVersion }
    try:
        response = requests.post(SW_VERSION_UPDATE_URL, data=data, verify=True, timeout=5)
    except requests.exceptions.RequestException as e:
        print e

def doUpdate():
    repo = git.Repo('/petiary/petd')
    org = repo.remotes.origin
    ret = org.pull()
    notifySWVersion(repo.head.commit.hexsha[0:7])
    time.sleep(3)
    print 'update result : %d' % (ret[0].flags)
    if ret[0].flags == 64:
        blinkLED()
        os.system('sync && reboot')

def doApMode():
    os.system('ifdown wlan0')
    GREEN(ON)
    RED(ON)
    time.sleep(3)
    os.system('/petiary/petifi/create_ap/create_ap -n wlan0 petiary petiary123 &')
    os.system('/usr/bin/nodejs /petiary/petifi/petifi/app.js &')

def execModules():
    os.system('motion -c /petiary/petd/motion.conf -l /petiary/petd/motion.log -p /petiary/petd/MotionPid')
    os.system('/usr/bin/python /petiary/petd/buttond.py &')
    os.system('/usr/bin/python /petiary/petd/scaled.py &')
    os.system('/usr/bin/python /petiary/petd/petd.py &')
    os.system('/usr/bin/python /petiary/petd/updated.py &')

def doNormalMode():
    checkCnt = 0
    while True:
        if isConnected():
            GREEN(ON)
            RED(OFF)
            doUpdate()
            execModules()
            break
        if checkCnt > 10:
            GREEN(OFF)
            RED(ON)
            break
        print 'WIFI connection failed : %d' % (checkCnt)
        time.sleep(2)
        checkCnt = checkCnt + 1

#==================== main loop ===================
init()
loadSerialKey()

state = readBtn()
if state == PRESS:
    firstPressed = time.time()
    while True:
        time.sleep(0.1)
        state = readBtn()
        if state == RELEASE:
            doNormalMode()
            break
        pressingTime = time.time() - firstPressed
        if pressingTime > 3:
            doApMode()
            break
else:
    doNormalMode()

