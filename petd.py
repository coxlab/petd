import os
import redis
import time
import requests
import json
from datetime import datetime
import subprocess
import signal

# constants
UPLOAD_URL = "https://petiary.coxlab.kr/upload"
READY = 0
RUNNING = 1

# global variables
state = 0
start_time = ""
start_gram = 0
serial_key = ""
motion_pid = ""
timeout_cnt = 0

# redis
r = redis.StrictRedis()
p = r. pubsub()
p.subscribe('petd')

#================== functions ===================#
def loadSerialKey():
    global serial_key
    try:
        f = open("/petiary/petd/SerialKey", 'r')
        serial_key = f.read().splitlines()[0]
        f.close()
    except:
        print "failed loading serial key" 

def loadMotionPid():
    global motion_pid
    try:
        f = open("/petiary/petd/MotionPid",'r')
        motion_pid = f.read().splitlines()[0]
        f.close()
    except:
        print "failed loading motion pid"

def postEvent(gram, movie):
    global start_time
    global serial_key
    movPath = os.path.dirname(movie)
    thumbnail = movPath + "/thumb.jpg"
    files = { 'movie' : open(movie), 'thumbnail' : open(thumbnail) }
    data = { 'sn' : serial_key, 'timestamp' : start_time, 'gram' : gram }
    try:
        response = requests.post(UPLOAD_URL, files=files, data=data, verify=True)
        subprocess.Popen(['-c', 'rm -rf %s' % movPath],stdout=subprocess.PIPE,shell=True).wait()
    except requests.exceptions.RequestException as e:
        print e

def deleteMovie(movie):
    movPath = os.path.dirname(movie)
    subprocess.Popen(['-c', 'rm -rf %s' % movPath],stdout=subprocess.PIPE,shell=True).wait()

#=================== main loop ===================#
loadSerialKey()
loadMotionPid()

while True:
    msg = p.get_message()
    if msg != None and msg['data'] != 1:
        data = json.loads(msg['data'])
        if data['state'] == 'start' and state == READY:
            timeout_cnt = 0
            state = RUNNING
            start_gram = int(r.get('prevGram'))
            start_time = datetime.now()
        elif data['state'] == 'reset' and state == RUNNING:
            timeout_cnt = 0
            start_gram = int(r.get('curGram'))
        elif data['state'] == 'end' and state == RUNNING:
            state = READY
            end_gram = int(r.get('curGram'))
            dif = end_gram - start_gram
            if dif < -1300:
                postEvent(int(round(dif/1000.0)), data['movie'])
            else:
                deleteMovie(data['movie'])
        else:
            state = READY

    if state == RUNNING:
        curGram = int(r.get('curGram'))
        if start_gram - curGram > -1300:
            timeout_cnt = timeout_cnt + 1
        if timeout_cnt > 100:
            os.kill(int(motion_pid), signal.SIGUSR1)
            timeout_cnt = 0
    time.sleep(0.1)

