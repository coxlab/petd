import redis
import time
import requests
from pyA20.gpio import gpio
from pyA20.gpio import port
import scalelib

serial_key = ""
prevGram = 0
curGram = 0
postGram = 0
OFFSET_FILE = '/petiary/petd/offset'

url = "https://petiary.coxlab.kr/update-scale"

r = redis.StrictRedis()
p = r.pubsub()
p.subscribe('scale')

#==================== functions ===================#
def init():
    scalelib.init()

def postToServer(gram):
    global serial_key
    data = { 'sn' : serial_key, 'curGram' : gram }
    try:
        response = requests.post(url, data=data, verify=True, timeout=30)
    except requests.exceptions.RequestException as e:
        print e

def loadSerialKey():
    global serial_key
    try:
        f = open("/petiary/petd/SerialKey", 'r')
        serial_key = f.read().splitlines()[0]
        print serial_key
        f.close()
    except:
        print "failed loading serial key"

#=================== main loop ===================#
init()
loadSerialKey()
while True:
    msg = p.get_message()
    if msg != None and msg['data'] != 1:
        if msg['data'] == 'reset':
            print 'adjustZero'
            scalelib.adjustZero()
    
    prevGram = curGram
    r.set('prevGram', prevGram)
    curGram = scalelib.read()
    if curGram < 0:
        curGram = 0
    r.set('curGram', curGram)
    if abs(postGram - curGram) > 1300:
        postGram = curGram
        postToServer(int(round(curGram/1000.0)))

    time.sleep(2)
