import os
import redis
import time
import json
from pyA20.gpio import gpio
from pyA20.gpio import port

# constants
ON = 0
OFF = 1
PRESS = 0
RELEASE = 1

# gpio
btn = port.PD14
redLED = port.PA6
greenLED = port.PA3

# redis
r = redis.StrictRedis()

#================== functions ===================#
def init():
    gpio.init()
    gpio.setcfg(btn, gpio.INPUT)
    gpio.setcfg(redLED, gpio.OUTPUT)
    gpio.setcfg(greenLED, gpio.OUTPUT)
    time.sleep(0.1)
    gpio.pullup(btn, gpio.PULLDOWN)

def RED(x):
    gpio.output(redLED, x)

def GREEN(x):
    gpio.output(greenLED, x)

def blinkGreen(n):
    for x in range(0, n):
        GREEN(ON)
        time.sleep(0.5)
        GREEN(OFF)
        time.sleep(0.5)
def blinkRed(n):
    for x in range(0, n):
        RED(ON)
        time.sleep(0.5)
        RED(OFF)
        time.sleep(0.5)

def readBtn():
    return gpio.input(btn)

#================== main loop ===================#
init()
while True:
    state = readBtn()
    if state == PRESS:
        first_pressed_time = time.time()
        time.sleep(1)
        state = readBtn()
        if state == RELEASE:
            r.publish('petd', json.dumps({ 'state' : 'reset' }))
            blinkGreen(3)
        else:
            while state == PRESS:
                time.sleep(0.2)
                state = readBtn()
                pressing_time = time.time() - first_pressed_time
                if pressing_time > 2:
                    r.publish('scale', 'reset')
                    blinkRed(3)
                    while state == PRESS:
                        time.sleep(0.2)
                        state = readBtn()
                    break
    time.sleep(0.001)

