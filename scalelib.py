import os
from time import sleep
from pyA20.gpio import gpio
from pyA20.gpio import port

slk = port.PC7
dout = port.PC4
led = port.PA6

SCALE = 742
AVG_TIMES = 32
g_offset = 0
OFFSET_FILE = '/petiary/petd/offset'

def init():
    gpio.init()
    gpio.setcfg(slk, gpio.OUTPUT)
    gpio.setcfg(dout, gpio.INPUT)
    gpio.setcfg(led, gpio.OUTPUT)

    gpio.output(slk, 1)
    sleep(0.1)
    gpio.output(slk, 0)
    loadOffset()

def loadOffset():
    global g_offset
    if os.path.isfile(OFFSET_FILE):
        f = open(OFFSET_FILE, 'r')
        try:
            g_offset = int(f.read())
        except Exception as ex:
            g_offset = 0
            f.close()
        f.close()

def adjustZero():
    global g_offset
    gpio.output(led, 0)
    g_offset = getAverage(AVG_TIMES)
    f = open(OFFSET_FILE, 'w')
    f.write(str(g_offset))
    f.close()
    gpio.output(led, 1)

def getAveragedMilligram(times):
    return (getAverage(times) - g_offset) * 1000 / SCALE

def getMilligram():
    return (getRaw() - g_offset) * 1000 / _SCALE

def getAverage(times):
    sum = 0
    valArr = []
    for i in range(0, times):
        val = getRaw()
        valArr.append(val)
    valArr.sort()
    for i in range(1, times-1):
        sum += valArr[i]
    return sum / (times-2)
    
def getRaw():
    data = [0,0,0]
    while gpio.input(dout):
        continue
    
    for i in range(2, -1, -1):
        for j in range(7, -1, -1):
            gpio.output(slk, 1)
            data[i] |= gpio.input(dout) << j
            gpio.output(slk, 0)
    
    gpio.output(slk, 1)
    gpio.output(slk, 0)

    data[2] ^= 0x80
    data[2] &= 0xFF

    return data[2] << 16 | data[1] << 8 | data[0] << 0

def read():
    return getAveragedMilligram(10)
    
