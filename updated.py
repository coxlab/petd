import os
import git
import time
import requests
from pyA20.gpio import gpio
from pyA20.gpio import port

# constants
ON = 0
OFF = 1
UPDATE_INTERVAL = 6 * 60 * 60 # 6 hours

# GPIO 
redLed = port.PA6
greenLed = port.PA3

#================== functions ===================
def init():
    gpio.init()
    gpio.setcfg(redLed, gpio.OUTPUT)
    gpio.setcfg(greenLed, gpio.OUTPUT)

def GREEN(x):
    gpio.output(greenLed, x)

def RED(x):
    gpio.output(redLed, x)

def blinkLED():
    for x in range(0, 5):
        GREEN(ON)
        RED(ON)
        time.sleep(0.5)
        GREEN(OFF)
        RED(OFF)
        time.sleep(0.5)

def doUpdate():
    repo = git.Repo('/petiary/petd')
    org = repo.remotes.origin
    ret = org.pull() 
    return ret[0].flags

#================== main loop ===================#
init()

while True:
    time.sleep(UPDATE_INTERVAL)
    ret = doUpdate()
    if ret == 64:
        blinkLED()
        os.system('sync && reboot')

